import React from 'react'
import Ciencia from '../../components/cienciaTecnologia/Ciencia'
import Navbar from '../../components/layout/Navbar'

const cienciaYTecnollogia = () => {
    return (
        <div>
            <Ciencia />
        </div>
    )
}

export default cienciaYTecnollogia
