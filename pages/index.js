import { Grid, Typography, makeStyles } from '@material-ui/core';
import React from 'react';
import Image from 'next/image';
import portada from '../public/assets/images/Home/pexels-pixabay-2166.jpg';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex'
	},
	image: {
		position: 'relative'
	},
	textTitle: {
		position: 'absolute',
		color: 'white',
		textAlign: 'end',
		marginLeft: '40%',
		marginTop: '10%'
	}
}));

const Home = () => {
	const classes = useStyles();
	return (
		<Grid container>
			<Grid item lg={12}>
				<Image className={classes.image} src={portada} alt="image" />
			</Grid>
			<Grid item lg={6} className={classes.textTitle}>
				<Typography variant="h2">Encuentra artículos de ciencia y tecnología de vanguardia</Typography>
			</Grid>
		</Grid>
	);
};

export default Home;
