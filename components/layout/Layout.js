import React, { Fragment } from 'react';
import Navbar from './Navbar';
import Head from 'next/head';
import MobilHeader from './MobilHeader';

import { Hidden, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
	toolbar: theme.mixins.toolbar,
	content: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.default
	}
}));

const layout = (props) => {
	const classes = useStyles();
	const [ open, setOpen ] = React.useState(false);

	const actionOpen = () => {
		setOpen(!open)
	}

	return (
		<Fragment>
			<Head>
				<title>Sinergia</title>
				<meta name="description" content="contenido para ñoños" />
			</Head>
			<Navbar actionOpen={actionOpen} />
			<Hidden mdUp>
				<MobilHeader variant="temporary" open={open} onClose={actionOpen}/>
			</Hidden>
			<div className={classes.content}>
				<div className={classes.toolbar} />
				<main>{props.children}</main>
			</div>
		</Fragment>
	);
};

export default layout;
