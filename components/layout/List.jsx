import React from 'react';
import Link from 'next/link';
import { List, ListItem, ListItemText, makeStyles } from '@material-ui/core';

const staleStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		flexDirection: 'row',
		[theme.breakpoints.down('sm')]: {
			display: 'flex',
			flexDirection: 'column'
		}
	},
	item: {
		width: '150px'
	}
}));

const MainHeader = () => {
	const classes = staleStyles();
	return (
		<div>
			<List componet="nav" className={classes.root}>
				<Link href="/">
					<ListItem button>
						<ListItemText primary="Home" />
					</ListItem>
				</Link>
				<Link href="/cienciaYTecnologia">
					<ListItem button>
						<ListItemText className={classes.item} primary="ciencia y tecnología" />
					</ListItem>
				</Link>
				<Link href="/EcologiaSocial">
					<ListItem button>
						<ListItemText
							style={{
								width: '120px'
							}}
						>
							Ecología social
						</ListItemText>
					</ListItem>
				</Link>
				<Link href="/Economia">
					<ListItem button>
						<ListItemText>Economía</ListItemText>
					</ListItem>
				</Link>
				<Link href="/BiologiaYMedicina">
					<ListItem button>
						<ListItemText className={classes.item}>Biología y medicina</ListItemText>
					</ListItem>
				</Link>
			</List>
		</div>
	);
};

export default MainHeader;
