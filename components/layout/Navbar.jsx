import React from 'react';
import List from './List';
import Link from 'next/link';
import { AppBar, Toolbar, useTheme, IconButton, Grid, useMediaQuery, makeStyles } from '@material-ui/core';
import Image from 'next/image';
import iconoNav from '../../public/assets/images/navbar/icons8-aprendizaje-60.png';
import MenuBookIcon from '@material-ui/icons/MenuBook';

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: '#B9770E'
	},
	iconNav: {
		flexGrow: 1
	},
	menuButton: {
		[theme.breakpoints.up('md')]: {
			display: 'none'
		}
	},
	listMenu: {
		display: 'flex',
		justifyContent: 'flex-end',
		[theme.breakpoints.down('sm')]: {
			display: 'none'
		}
	}
}));

const Navbar = (props) => {
	const classes = useStyles();
	const theme = useTheme();
	const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
	return (
		<AppBar className={classes.root}>
			<Toolbar>
				<Grid container>
					<Grid item className={classes.iconNav} md={1} lg={1}>
						<Link href="/">
							<Image src={iconoNav} alt="icon" />
						</Link>
					</Grid>
					{isMobile ? (
						<Grid item md={1}>
							<IconButton
								onClick={() => props.actionOpen()}
								className={classes.menuButton}
								color="inherit"
								aria-label="menu"
							>
								<MenuBookIcon />
							</IconButton>
						</Grid>
					) : (
						<Grid item sm={10} md={11} lg={11} className={classes.listMenu}>
							<List />
						</Grid>
					)}
				</Grid>
			</Toolbar>
		</AppBar>
	);
};

export default Navbar;
