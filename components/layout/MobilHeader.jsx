import React from 'react';
import { makeStyles, Drawer, Divider } from '@material-ui/core';
import List from './List';

const useStyles = makeStyles((theme) => ({
	drawer: {
		width: 240,
		flexShrink: 0
	},
	drawerPaper: {
		width: 240
	},
	toolbar: theme.mixins.toolbar
}));

const MobilHeader = (props) => {
	const classes = useStyles();
	return (
		<Drawer
			className={classes.drawer}
			classes={{
				paper: classes.drawerPaper
			}}
			anchor="right"
			variant={props.variant}
			open={props.open}
			onClose={props.onClose}
		>
			<div className={classes.toolbar} />
			<Divider />
			<List />
		</Drawer>
	);
};

export default MobilHeader;
